---
lastmod: 2021-05-21
title: Download Full Node Wallet
titleOnlyInHead: 1
---

## Core Wallet

Core Wallet is implemented as a full node that provides a builtin wallet with web UI and RPC API.

* Download and unzip the latest release.
* Change directory to `blacknet/bin`
* On UN*X run `./blacknet`
* On Windows run `.\blacknet.bat`
* Web interface is available at [http://localhost:8283/](http://localhost:8283/)

[<i class="fas fa-file-archive"></i> Download v0.2.11](https://gitlab.com/blacknet-ninja/blacknet/-/jobs/1281965857/artifacts/download)

Release type `Migration to Tor version 3`

Release date `May 21 2021`

MD5 checksum `E76CC874406CFF9E09546E18F1874A46`

### Prerequisite

<i class="fab fa-java"></i> Java 8 or higher.

- Debian & Ubuntu: `sudo apt-get install default-jre`
- Red Hat & Oracle: `sudo yum install java-11-openjdk`
- SUSE: `sudo zypper install java-11-openjdk`
- Arch: `sudo pacman -S --needed jre-openjdk`
- Gentoo: `sudo emerge -av1 --noreplace virtual/jre`
- FreeBSD: `sudo pkg install openjdk11-jre`
- OpenBSD: `sudo pkg_add jdk`
- macOS & Windows: [OpenJDK](https://jdk.java.net/), [Oracle Java](https://java.com/download/), or other

## Source code

Download the source code for building locally.
For build instructions see the README file.

[<i class="fas fa-file-archive"></i> Download v0.2.11](https://gitlab.com/blacknet-ninja/blacknet/-/archive/v0.2.11/blacknet-v0.2.11.zip)

[<i class="fas fa-tags"></i> Download previous release tags](https://gitlab.com/blacknet-ninja/blacknet/-/tags)
