---
lastmod: 2020-10-06
title: Wallet
titleOnlyInHead: 1
---

# Software wallet

Software cryptocurrency wallet is a program that stores keys and transactions, can sign new transactions.
Text message signing for off-chain communication is also offered.
Core Wallet runs on a variety of operating systems.
Desktop Wallet is available for macOS and Windows.
Mobile Wallet is on Google Play and iOS App Store.
APK is also there for Android.

## Core Wallet

[<i class="fas fa-download"></i> Download Page](download.html)


## Desktop Wallet

[<i class="fab fa-apple"></i> macOS](https://gitlab.com/blacknet-ninja/blacknet-desktop/-/releases)

[<i class="fab fa-windows"></i> Windows](https://gitlab.com/blacknet-ninja/blacknet-desktop/-/releases)


## Mobile Wallet

[<i class="fab fa-google-play"></i> Google Play](https://play.google.com/store/apps/details?id=ninja.blacknet.wallet.blacknet)

[<i class="fab fa-android"></i> Android APK](https://gitlab.com/blacknet-ninja/blacknet-mobile/-/releases)

[<i class="fab fa-app-store-ios"></i> iOS](https://apps.apple.com/app/blacknet/id1489451592)
